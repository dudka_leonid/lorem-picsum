package com.e.lorempicsum.view.main.allimage

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.service.ItemImageManager
import com.e.lorempicsum.service.LoremPicsumServiceFactory
import kotlinx.coroutines.launch

class AllImageViewModel: ViewModel() {

    private var allImageRecycler: AllImageRecycler? = null
    private var itemImageManager: ItemImageManager? = null

    private fun getImages(
        page: String,
        limit: String,
        onLoading: () -> Unit = {},
        onComplete: (List<ItemImage>) -> Unit = {},
        onError: (Exception) -> Unit = {},
    ) {
        viewModelScope.launch {
            try {
                val response =
                    LoremPicsumServiceFactory.service.getListImageAsync(page, limit)
                val result = response.await()
                onComplete(result.body()!!)
            } catch (e: Exception) {
                onError(e)
            }
        }
    }

    fun initAllImageRecycler(
        allImageRecycler: AllImageRecycler,
        itemImageManager: ItemImageManager
    ) {
        this.allImageRecycler = allImageRecycler
        this.itemImageManager = itemImageManager
        this.allImageRecycler!!.addResource { page, limit ->
            this.getImages(page, limit,
                onComplete = {
                    this.itemImageManager!!.addItemImages(it)
                },
                onError = {})
        }
        val key = "asd3243asfd"
        itemImageManager.addObserver(key, object : ItemImageManager.Listener{
            override fun onNotLikeToLike(id: String) {
            }

            override fun onLikeToNotLike(id: String) {
                allImageRecycler.allImageAdapter.onNotLike(id)
            }

            override fun onChange() {
                allImageRecycler.allImageAdapter.notifyDataSetChanged()
            }
        })
        this.allImageRecycler!!.start()
    }
}