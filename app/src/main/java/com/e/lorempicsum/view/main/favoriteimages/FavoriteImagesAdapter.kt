package com.e.lorempicsum.view.main.favoriteimages

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.e.lorempicsum.R
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.service.LoremPicsumURLS

class FavoriteImagesAdapter(
    private val context: Context,
    private val imageItems: HashMap<String, ItemImage>
): RecyclerView.Adapter<FavoriteImagesAdapter.ViewHolder>() {

    private val keys: MutableSet<String> = imageItems.keys

    var onItemClick: ((ItemImage)->Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_image, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return imageItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(imageItems[keys.elementAt(position)]!!)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val likeImage: ImageView = itemView.findViewById(R.id.like_indicator_image_view_id)
        private val authorTextView: TextView = itemView.findViewById(R.id.author_text_view_id)
        private val imageImageView: ImageView = itemView.findViewById(R.id.item_image_image_view_id)

        fun bind(itemImage: ItemImage) {
            onLoadImage(itemImage)
            authorTextView.text = itemImage.author

            likeImage.setColorFilter(Color.parseColor("#E12C2C"))
            likeImage.setOnClickListener {
                likeImage.setColorFilter(Color.parseColor("#656565"))
                onItemClick?.invoke(itemImage)
            }
        }

        private fun onLoadImage(itemImage: ItemImage) {
            val imageUrl = Uri
                .parse(
                    LoremPicsumURLS
                        .specificImage(itemImage.id, "300", "200"))

            Glide
                .with(context)
                .load(imageUrl)
                .placeholder(R.drawable.ic_launcher_foreground)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(imageImageView)
        }
    }
}