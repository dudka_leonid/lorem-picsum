package com.e.lorempicsum.view.main

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.e.lorempicsum.R
import com.e.lorempicsum.service.ItemImageManager
import com.e.lorempicsum.service.RealmService
import com.e.lorempicsum.view.main.allimage.AllImageFragment
import com.e.lorempicsum.view.main.favoriteimages.FavoriteImagesFragment


class MainActivity : FragmentActivity() {
    private val STORAGE_PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private lateinit var itemImageManager: ItemImageManager
    private lateinit var fragments: List<Fragment>
    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        verifyPermissions()

        this.itemImageManager = ItemImageManager(RealmService(this))
        this.fragments = listOf(
            AllImageFragment(this.itemImageManager),
            FavoriteImagesFragment(this.itemImageManager)
        )

        this.viewPager = findViewById(R.id.pager)
        val pagerAdapter = ScreenSlidePagerAdapter(this.fragments, this)
        this.viewPager.adapter = pagerAdapter
    }

    private class ScreenSlidePagerAdapter(
        val fragments: List<Fragment>,
        fa: FragmentActivity
    ) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = fragments.size
        override fun createFragment(position: Int): Fragment = fragments[position]
    }

    private fun verifyPermissions() {
        // This will return the current Status
        val permissionExternalMemory = ActivityCompat.checkSelfPermission(
            this@MainActivity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (permissionExternalMemory != PackageManager.PERMISSION_GRANTED) {
            // If permission not granted then ask for permission real time.
            ActivityCompat.requestPermissions(this@MainActivity, STORAGE_PERMISSIONS, 1)
        }
    }

    override fun onPause() {
        itemImageManager.saveData {
            super.onPause()
        }
    }
}