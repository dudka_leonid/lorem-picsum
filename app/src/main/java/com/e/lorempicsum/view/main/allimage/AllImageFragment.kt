package com.e.lorempicsum.view.main.allimage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.e.lorempicsum.R
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.service.ItemImageManager

class AllImageFragment(
    private val itemImageManager: ItemImageManager
) : Fragment() {

    private lateinit var viewModel: AllImageViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.all_image_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AllImageViewModel::class.java)

        val builder = AllImageRecyclerBuilder
            .setAdapter(AllImageAdapter(requireContext(), itemImageManager.allImages))
            .setLikeImageClickEvent { _: ImageView, itemImage: ItemImage, isLike: Boolean ->
                val message = if (isLike) {
                    itemImageManager.onLike(itemImage)
                    "Вам понравилось изображение #${itemImage.id}"
                } else {
                    itemImageManager.onNotLike(itemImage)
                    "Вам перестала нравится изображение #${itemImage.id}"
                }
                Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
            }
            .setRecycler(requireView().findViewById(R.id.images_container_recycler_view_id))
        viewModel.initAllImageRecycler(builder.build(), itemImageManager)
    }

}