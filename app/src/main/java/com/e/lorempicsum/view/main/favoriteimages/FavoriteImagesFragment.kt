package com.e.lorempicsum.view.main.favoriteimages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.e.lorempicsum.R
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.service.ItemImageManager

class FavoriteImagesFragment(
    private val itemImageManager: ItemImageManager
) : Fragment() {

    private lateinit var viewModel: FavoriteImagesViewModel
    private lateinit var adapter: FavoriteImagesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater
            .inflate(R.layout.favorite_images_fragment, container, false)
        adapter = FavoriteImagesAdapter(requireContext(), itemImageManager.favoriteImages)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FavoriteImagesViewModel::class.java)


        val recycler: RecyclerView =
            requireView().findViewById(R.id.images_container_recycler_view_id)
        adapter.onItemClick = { itemImage: ItemImage ->
            val message = "Вам перестала нравится изображение #${itemImage.id}"
            Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
            itemImageManager.onNotLike(itemImage)
        }
        val key = "sadf43r5"
        itemImageManager.addObserver(key, object : ItemImageManager.Listener{
            override fun onNotLikeToLike(id: String) {
                adapter.notifyDataSetChanged()
            }

            override fun onLikeToNotLike(id: String) {
                adapter.notifyDataSetChanged()
            }

            override fun onChange() {
                adapter.notifyDataSetChanged()
            }
        })
        recycler.adapter = adapter
    }
}