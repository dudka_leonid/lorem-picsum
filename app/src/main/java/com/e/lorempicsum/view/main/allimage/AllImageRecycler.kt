package com.e.lorempicsum.view.main.allimage

import android.util.Log
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.utils.EndlessRecyclerOnScrollListener

class AllImageRecycler{

    companion object {
        const val PAGE_DEFAULT = 1L
        const val LIMIT_DEFAULT = 20L
    }

    var recyclerView: RecyclerView
        private set
    var allImageAdapter: AllImageAdapter
        private set
    var page: Long
        private set
    var limit: Long
        private set
    private var getSource: ((String, String) -> Unit)? = null

    constructor(
        recyclerView: RecyclerView,
        allImageAdapter: AllImageAdapter
    ) : this(recyclerView, allImageAdapter, { _: ImageView, _: ItemImage, _: Boolean -> })

    constructor(
        recyclerView: RecyclerView,
        allImageAdapter: AllImageAdapter,
        function: (ImageView, ItemImage, Boolean) -> Unit
    ) {
        this.allImageAdapter = allImageAdapter
        this.recyclerView = recyclerView

        this.page = PAGE_DEFAULT
        this.limit = LIMIT_DEFAULT
        initComponents(function)
    }

    private fun initComponents(function: (ImageView, ItemImage, Boolean) -> Unit) {
        allImageAdapter.onItemClick = function
        recyclerView.adapter = allImageAdapter

        recyclerView.addOnScrollListener(object : EndlessRecyclerOnScrollListener(){
            override fun onLoadMore() {
                this@AllImageRecycler.page = page + 1
                start()
            }
        })
    }

    fun addResource(function: (String, String) -> Unit) {
        this.getSource = function
    }

    fun start() {
        getSource?.invoke(page.toString(), limit.toString())
    }

}