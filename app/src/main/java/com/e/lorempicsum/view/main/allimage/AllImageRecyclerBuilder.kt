package com.e.lorempicsum.view.main.allimage

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.utils.isNotNull
import com.e.lorempicsum.utils.isNull

object AllImageRecyclerBuilder {

    private const val message = "(AllImageRecyclerBuilder.build())"

    private var allImageAdapter: AllImageAdapter? = null
    private var function: ((ImageView, ItemImage, Boolean) -> Unit)? = null
    private var recyclerView: RecyclerView? = null

    fun setAdapter(allImageAdapter: AllImageAdapter): AllImageRecyclerBuilder {
        this.allImageAdapter = allImageAdapter
        return this
    }

    fun setLikeImageClickEvent(
        function: (ImageView, ItemImage, Boolean) -> Unit
    ): AllImageRecyclerBuilder {
        this.function = function
        return this
    }

    fun setRecycler(recyclerView: RecyclerView): AllImageRecyclerBuilder {
        this.recyclerView = recyclerView
        return this
    }

    fun build(): AllImageRecycler {
        return if (recyclerView.isNotNull() && allImageAdapter.isNotNull()) {
            if (function.isNull()) {
                AllImageRecycler(recyclerView!!, allImageAdapter!!)
            } else {
                AllImageRecycler(recyclerView!!, allImageAdapter!!, function!!)
            }
        } else {
            throw NullPointerException(message)
        }
    }
}