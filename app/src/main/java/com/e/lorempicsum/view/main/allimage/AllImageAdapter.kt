package com.e.lorempicsum.view.main.allimage

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.e.lorempicsum.R
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.service.LoremPicsumURLS


class AllImageAdapter(
    private val context: Context,
    private val imageItems: ArrayList<ItemImage>
): RecyclerView.Adapter<AllImageAdapter.ViewHolder>() {

    var onItemClick: ((ImageView, ItemImage, Boolean)->Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_image, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return imageItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(imageItems[position])
    }

    fun onNotLike(id: String) {
        for (i in imageItems.indices) {
            if (imageItems[i].id == id) {
                imageItems[i].isLike = false
                this.notifyItemChanged(i)
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val likeImage: ImageView = itemView.findViewById(R.id.like_indicator_image_view_id)
        private val authorTextView: TextView = itemView.findViewById(R.id.author_text_view_id)
        private val imageImageView: ImageView = itemView.findViewById(R.id.item_image_image_view_id)

        fun bind(itemImage: ItemImage) {
            onLoadImage(itemImage)
            authorTextView.text = itemImage.author

            if (itemImage.isLike) {
                likeImage.setColorFilter(Color.parseColor("#E12C2C"))
                likeImage.setOnClickListener {
                    onFavoriteToNotFavorite(itemImage)
                }
            } else {
                likeImage.setColorFilter(Color.parseColor("#656565"))
                likeImage.setOnClickListener {
                    onNotFavoriteToFavorite(itemImage)
                }
            }
        }

        private fun onLoadImage(itemImage: ItemImage) {
            val imageUrl = Uri
                .parse(
                    LoremPicsumURLS
                    .specificImage(itemImage.id, "300", "200"))

            Glide
                .with(context)
                .load(imageUrl)
                .placeholder(R.drawable.ic_launcher_foreground)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(imageImageView)
        }

        private fun onNotFavoriteToFavorite(itemImage: ItemImage) {
            itemImage.isLike = true
            likeImage.setColorFilter(Color.parseColor("#E12C2C"))
            onItemClick?.invoke(likeImage, itemImage, itemImage.isLike)
            likeImage.setOnClickListener { onFavoriteToNotFavorite(itemImage) }
        }

        private fun onFavoriteToNotFavorite(itemImage: ItemImage) {
            itemImage.isLike = false
            likeImage.setColorFilter(Color.parseColor("#656565"))
            onItemClick?.invoke(likeImage, itemImage, itemImage.isLike)
            likeImage.setOnClickListener { onNotFavoriteToFavorite(itemImage) }
        }
    }
}
