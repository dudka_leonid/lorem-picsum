package com.e.lorempicsum.service

// You can find a list of all the images https://picsum.photos/images.
object LoremPicsumURLS {
    private const val basic = "https://picsum.photos"

    /**
     * Just add your desired image size (width & height)
     * after our URL, and you'll get a random image.
     */
    fun getBasicImage(width: String, height: String) = "$basic/$width/$height"

    /**
     * To get a square image, just add the size.
     */
    fun getSquareImage(size: String) = "$basic/$size"

    /**
     * Get a specific image by adding /id/{image} to the start of the url.
     */
    fun specificImage(id: String, width: String, height: String) = "$basic/id/$id/$width/$height"
}