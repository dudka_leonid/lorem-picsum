package com.e.lorempicsum.service

import com.e.lorempicsum.model.ItemImage

class ItemImageManager(private val realmService: RealmService) {

    val favoriteImages = HashMap<String, ItemImage>()
    val allImages = ArrayList<ItemImage>()

    private val observers: HashMap<String, Listener> = HashMap()

    interface Listener {
        fun onNotLikeToLike(id: String)
        fun onLikeToNotLike(id: String)
        fun onChange()
    }

    init {
        realmService.readData { items ->
            items.forEach { item ->
                favoriteImages[item.id] = item
            }
        }
    }

    fun saveData(onPause: () -> Unit) {
        favoriteImages.forEach {
            realmService.saveData(it.value)
        }
        onPause.invoke()
    }

    fun onLike(itemImage: ItemImage) {
        this.favoriteImages[itemImage.id] = itemImage
        onLikeObservers(itemImage.id)
    }

    fun onNotLike(itemImage: ItemImage) {
        val id = itemImage.id
        onNotLikeObservers(id)
        this.favoriteImages.remove(itemImage.id)
        this.realmService.deleteData(id)
    }

    fun addItemImages(it: List<ItemImage>) {
        for (i in it.indices) {
            if (it[i].id == favoriteImages[it[i].id]?.id) {
                it[i].isLike = true
            }
        }
        this.allImages.addAll(it)
        onChangeObservers()
    }

    private fun onChangeObservers() {
        this.observers.forEach {
            it.value.onChange()
        }
    }

    private fun onLikeObservers(id: String) {
        this.observers.forEach {
            it.value.onNotLikeToLike(id)
        }
    }

    private fun onNotLikeObservers(id: String) {
        this.observers.forEach {
            it.value.onLikeToNotLike(id)
        }
    }

    fun addObserver(key: String, observer: Listener) {
        this.observers[key] = observer
    }

    fun removeObserver(key: String) {
        this.observers.remove(key)
    }
}