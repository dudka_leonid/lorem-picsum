package com.e.lorempicsum.service

import com.e.lorempicsum.model.ItemImage
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ILoremPicsumService {

    /**
     * To request another page, use the ?page parameter.
     * To change the amount of items per page, use the ?limit parameter.
     */
    @GET("/v2/list")
    fun getListImageAsync(
        @Query("page") page: String,
        @Query("limit") limit: String
    ): Deferred<Response<List<ItemImage>>>
}