package com.e.lorempicsum.service

import android.content.Context
import android.util.Log
import com.e.lorempicsum.model.ItemImage
import com.e.lorempicsum.service.RealmService.ResultListener
import io.realm.Realm
import io.realm.RealmResults


class RealmService {

    private val realm: Realm
    private var resultListener: ResultListener = ResultListener { }

    constructor(context: Context) {
        Realm.init(context)
        realm = Realm.getDefaultInstance()
    }

    constructor(realm: Realm) {
        this.realm = realm
    }

    fun setResultListener(resultListener: ResultListener) {
        this.resultListener = resultListener
    }

    fun removeResultListener() {
        this.resultListener = ResultListener { }
    }

    fun saveData(data: ItemImage) {
        realm.executeTransactionAsync({
            val itemImage = it.createObject(ItemImage::class.java)
            itemImage.id = data.id
            itemImage.author = data.author
            itemImage.width = data.width
            itemImage.height = data.height
            itemImage.url = data.url
            itemImage.download_url = data.download_url
            itemImage.isLike = data.isLike
        }, {}, {})
    }

    fun readData(listener: ResultListener) {
        val result = realm.where(ItemImage::class.java).findAll()
        val students = ArrayList<ItemImage>()
        result.forEach {
            students.add(it)
        }
        listener.onResult(students)
    }

    fun deleteData(dataId: String) {
        realm.executeTransaction { realm ->
            try {
                val result: RealmResults<ItemImage> =
                    realm.where(ItemImage::class.java).equalTo("id", dataId).findAll()
                result.deleteAllFromRealm()
            } catch (e: Exception) {

            }
        }
    }

    fun interface ResultListener {
        fun onResult(response: ArrayList<ItemImage>)
    }
}