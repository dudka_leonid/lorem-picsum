package com.e.lorempicsum.service

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object LoremPicsumServiceFactory {
    private const val baseURL = "https://picsum.photos"
    private val interceptor = Interceptor { chain->
        val request: Request =
            chain.request().newBuilder().addHeader("key", "value").build()
        chain.proceed(request)
    }
    private fun retrofit() : Retrofit = Retrofit.Builder()
        .client(OkHttpClient().newBuilder().addInterceptor(interceptor).build())
        .baseUrl(baseURL)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val service : ILoremPicsumService = retrofit()
        .create(ILoremPicsumService::class.java)
}