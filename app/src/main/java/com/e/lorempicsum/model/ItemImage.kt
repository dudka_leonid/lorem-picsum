package com.e.lorempicsum.model

import io.realm.RealmObject
import java.io.Serializable

open  class ItemImage (
    var id: String = "",
    var author: String = "",
    var width: String = "",
    var height: String = "",
    var url: String = "",
    var download_url: String = "",
    var isLike: Boolean = false
) : RealmObject(), Serializable